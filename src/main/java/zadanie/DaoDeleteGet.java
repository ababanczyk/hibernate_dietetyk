package zadanie;

import lombok.extern.java.Log;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
@Log
public class DaoDeleteGet {
    public <T> List<T> getAll(Class<T> tClass){
        // pobieramy fabrykę (do tworzenia sesji)
        SessionFactory factory = HibernateUtil.getSessionFactory();
        try(Session session = factory.openSession()){
            String nazwaKlasy = tClass.getSimpleName();
            Query<T> query = session.createQuery("from " + nazwaKlasy + " o", tClass);
            List<T> objectList= query.list();
            return objectList;
        } catch (HibernateException he){
            log.log(Level.SEVERE, "Error loading object.");
        }
        return new ArrayList<>();
    }
}
