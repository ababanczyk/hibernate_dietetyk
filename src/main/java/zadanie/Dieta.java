package zadanie;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Dieta extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id; //AUTO_INCREMENT
    private String nazwa;
    private double kcal;
    @Enumerated(EnumType.STRING)
    private TypDiety typDiety;

    public Dieta(String nazwa, double kcal, TypDiety typDiety, double ocena) {
        this.nazwa = nazwa;
        this.kcal = kcal;
        this.typDiety = typDiety;
        this.ocena = ocena;
    }

    private double ocena;

    @OneToMany(mappedBy = "dieta")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<Uzytkownik> uzytkownicy = new HashSet<>();
}
