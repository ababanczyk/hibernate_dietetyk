package zadanie;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        DaoCreateUpdate daoCreateUpdate = new DaoCreateUpdate();
        DaoDeleteGet daoDeleteGet = new DaoDeleteGet();

        Scanner scanner = new Scanner(System.in);

        String command;
        do {
            System.out.println("Podaj komendę:");
            command = scanner.next();

            if (command.equalsIgnoreCase("quit")) {
                //kończymy program po wpisaniu quit

                break;
            }
            // obsługa pozostałych komend:
            switch (command) {
                case "dodaj_trenera":
                    System.out.println("Podaj imię trenera");
                    String imie = scanner.next();
                    System.out.println("Podaj nazwisko trenera");
                    String nazwisko = scanner.next();
                    System.out.println("Podaj ocenę trenera");
                    double ocena = scanner.nextDouble();
                    System.out.println("Wskaż, czy trener jest dietetykiem");
                    boolean czyDietetyk = scanner.nextBoolean();

                    Trener trener = new Trener(imie, nazwisko, ocena, czyDietetyk);
                    daoCreateUpdate.saveEntity(trener);
                    break;
                case "dodaj_uzytkownika":
                    System.out.println("Podaj imię uzytkownika");
                    imie = scanner.next();
                    System.out.println("Podaj nazwisko uzytkownika");
                    nazwisko = scanner.next();
                    Uzytkownik uzytkownik = new Uzytkownik(imie, nazwisko);

                    daoCreateUpdate.saveEntity(uzytkownik);
                    break;
                case "dodaj_diete":
                    try {
                        System.out.println("Podaj nazwe");
                        String nazwa = scanner.next();
                        System.out.println("Podaj kalorycznosc");
                        double kcal = scanner.nextDouble();
                        System.out.println("Podaj typ diety");
                        String typ = scanner.next();
                        TypDiety typDiety = TypDiety.valueOf(typ.toUpperCase().trim());
                        System.out.println("Podaj ocenę diety");
                        ocena = scanner.nextDouble();

                        Dieta dieta = new Dieta(nazwa, kcal, typDiety, ocena);
                        daoCreateUpdate.saveEntity(dieta);
                        break;
                    } catch (IllegalArgumentException iae) {
                        System.out.println("Zła wartość argumentu, pomijam dodawanie.");
                    }
                    break;
                case "przypisz_diete":
                    System.out.println("Podaj imię uzytkownika");
                    imie = scanner.next();
                    System.out.println("Podaj nazwisko uzytkownika");
                    nazwisko = scanner.next();
                    System.out.println("Podaj typ diety");
                    String typ = scanner.next();
                    TypDiety typDiety = TypDiety.valueOf(typ.toUpperCase().trim());
                    System.out.println("Podaj kalorycznosc");
                    double kcal = scanner.nextDouble();
                default:
                    break;
            }

        } while (!command.equalsIgnoreCase("quit"));

//        daoCreateUpdate.saveEntity(new Diet("Keto 2000", 2000, DietType.KETO, 4.9));
//        daoCreateUpdate.saveEntity(new Diet("Standard 2500", 2500, DietType.STANDARD, 5.2));
//        daoCreateUpdate.saveEntity(new Diet("Sport 3000", 3000, DietType.SPORT, 5.0));
//
//        daoCreateUpdate.saveEntity(new Trainer("Pawel", "Gawel", 1.0, true));
//
//        daoCreateUpdate.saveEntity(new User("Gawel", "Pawel"));

        HibernateUtil.getSessionFactory().close();
    }
}
//        daoCreateUpdate.saveEntity(new Dieta("Keto 2000", 2000, TypDiety.KETO, 4));
//        daoCreateUpdate.saveEntity(new Dieta("Standard 2500", 2500, TypDiety.STANDARD, 4));
//        daoCreateUpdate.saveEntity(new Dieta("Sport 3000", 2000, TypDiety.SPORT, 3));
//        daoCreateUpdate.saveEntity(new Dieta("Wege 2000", 2000, TypDiety.WEGE, 5));
//
//        daoCreateUpdate.saveEntity(new Trener("Pawel", "Gawel", 1.0, true));
//        daoCreateUpdate.saveEntity(new Uzytkownik("Marek", "Zegarek"));

            //HibernateUtil.getSessionFactory().close();




