package zadanie;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Uzytkownik extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id; //AUTO_INCREMENT
    private String imie;
    private String nazwisko;

    public Uzytkownik(String imie, String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    @ManyToOne
    private Dieta dieta;

    @ManyToMany(mappedBy = "uzytkownicy")
    @ToString.Exclude //adnotacja, żeby nie było iteracyjnego zapętlenia
    @EqualsAndHashCode.Exclude
    private Set<Trener> trenerzy = new HashSet<>();  //przy większej ilości powiazań lepiej użyć SET


}
