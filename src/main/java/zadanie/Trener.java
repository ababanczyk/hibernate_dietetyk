package zadanie;

import java.util.*;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Trener extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)

    private Long id; //AUTO_INCREMENT // teraz dopiszę coś, żeby móc zapisać zmiany do gita
    private String imie;
    private String nazwisko;
    private double ocena;
    private boolean czyDietetyk;

    public Trener(String imie, String nazwisko, double ocena, boolean czyDietetyk) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.ocena = ocena;
        this.czyDietetyk = czyDietetyk;
    }

    @ManyToMany
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<Uzytkownik> uzytkownicy = new HashSet<>();

}
